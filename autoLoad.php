<?php

function autoLoading($class)
{


    $classPath = "$class.php";
    if (file_exists($classPath) and is_readable($classPath)) {
        include $classPath;
    }
}


spl_autoload_register('autoLoading');