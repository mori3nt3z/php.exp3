<?php

namespace App\Services\Router;

class Router
{

    public static $routes;

    public static function start()
    {
        //get all router from routes directory
        self::$routes = include BASE_PATH . "routes/web.php";
//        var_dump($currentRoad);
        //get current route
        $currentRoad = self::getCurrentRoute();
//        var_dump($currentRoad);


        // check current route exists
        if (self::routeExists($currentRoad)) {

            // is allowed method?
            if (!in_array(strtolower($_SERVER['REQUEST_METHOD']), self::getRouteMethod($currentRoad))) {

                header('HTTP/1.0 403 Forbidden');
                echo "403.html";

                die();
            }
        //get Current target
            $target = self::getRouteTarget($currentRoad);
//            var_dump($target);
        } else {
            header("HTTP/1.0 404 Not Found");
            echo "404.html";
        }
        //call method Controller
        list($controller,$method) =explode('@',$target); // ['controller','method of controller']
        var_dump($controller,$method);

    }

    public static function getCurrentRoute()
    {
        return strtok(strtolower($_SERVER['REQUEST_URI']), '?');
    }

    public static function getRouteTarget($rout)
    {
        return self::$routes[$rout]['target'];
    }

    public static function getRouteMethod($rout)
    {
        return explode('|', self::$routes[$rout]['method']);
    }

    public static function routeExists($rout)
    {

        return array_key_exists($rout, self::$routes);
    }
}