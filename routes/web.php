<?php

return [

    '/' => [

        'method' => 'get',
        'target' => 'HomeController@index'

    ],
    '/archive' => [

        'method' => 'get|post',
        'target' => 'PostController@archive'

    ],
];